<?php

namespace Drupal\smart_date\Plugin\FullcalendarViewProcessor;

use Drupal\fullcalendar_view\Plugin\FullcalendarViewProcessorBase;

/**
 * Smart Date plugin.
 *
 * @FullcalendarViewProcessor(
 *   id = "fullcalendar_view_smart_date",
 *   label = @Translation("Smart date processor"),
 *   field_types = {
 *     "smartdate"
 *   }
 * )
 */
class SmartDateProcessor extends FullcalendarViewProcessorBase {

  /**
   * @inheritDoc
   *
   * processing view results of fullcalendar_view for a smart date field
   *
   * ToDo:
   * - handling of drag and drop updates full calendar
   * - timezone handling
   * - smart date recurring events handling (should be roughly working)
   * - fullcalender_view recurring events handling (not considered yet, maybe
   * this is not to be supported by smart date anyway
   */
  public function process(array &$variables) {
    $view = $variables['view'];
    $fields = $view->field;
    $options =  $view->style_plugin->options;
    $start_field = $options['start'];
    $start_field_options = $fields[$start_field]->options;
    $multiple = $fields[$start_field]->multiple;
    // If not a Smart Date field or not existing config, nothing to do.
    if (strpos($start_field_options['type'], 'smartdate') !== 0 || empty($variables['#attached']['drupalSettings']['calendar_options'])) {
      return;
    }
    $calendar_options = json_decode($variables['#attached']['drupalSettings']['calendar_options'], TRUE);
    $entries = $calendar_options['events'];
    if ($multiple && $start_field_options['group_rows'] == TRUE) {
      $messenger = \Drupal::messenger();
      $messenger->addMessage('Do not group rows', $messenger::TYPE_WARNING);
    }
    else {
      foreach ($view->result as $key => $row) {
        $current_entity = $row->_entity;
        if ($multiple) {
          $type = $row->_entity->getEntityTypeId();
          $delta_key = $type . '__' . $start_field . '_delta';
          $delta = $row->{$delta_key};
          $rule_key = $type . '__' . $start_field . '_' . $start_field . '_rrule';
          $rule = $row->{$rule_key};
        }
        else {
          $delta = 0;
        }
        $values = $current_entity->get($start_field)->getValue();
        if (!empty($values)) {
          $start = $values[$delta]['value'];
          $end = $values[$delta]['end_value'];
          $rruleindex = $values[$delta]['rrule_index'];
          if (!empty($start)) {
            // Check for all day events.
            if (date('Hi', $start) == "0000" && date('Hi', $end) == "2359") {
              $start = date('Y-m-d', $start);
              // The end date is inclusive for a all day event in full calendar,
              // which is not what we want. So we need one day offset.
              $end = date('Y-m-d', $end + (60 * 60 * 24));
              $entries[$key]['allDay'] = TRUE;
            }
            else {
              $start = date(DATE_ATOM, $start);
              $end = date(DATE_ATOM, $end);
              $entries[$key]['allDay'] = FALSE;
            }
            $entries[$key]['start'] = $start;
            $entries[$key]['end'] = $end;
            $entries[$key]['delta'] = $delta;
            // Append the id with necessary additional data.
            if (!empty($rule)) {
              $entries[$key]['id'] = $current_entity->id() . '-R-' . $rule . '-I-' .$rruleindex;
            }
            else {
              $entries[$key]['id'] = $current_entity->id() . '-D-' . $delta;
            }
          }
        }
      }
    }
    // Update the entries
    if ($entries) {
      $calendar_options['events'] = $entries;
      $variables['#attached']['drupalSettings']['calendar_options'] = json_encode($calendar_options);
    }
  }

}
